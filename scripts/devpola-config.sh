#!/bin/bash
# /dev/pola Configuration

#    devpola-config
#    Copyright (C) 2017  ringbuchblock
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# If enabled additional logs will be written.
DEBUG=false


#################################################
# /dev/pola/ Web Site
#################################################
DEVPOLA_URI_SHORT="devpola.devlol.org"
DEVPOLA_URI="https://"$DEVPOLA_URI_SHORT"/"


#################################################
# GPIO pin allocation
#################################################

# mandatory
SHUTTER=22

# optional
INFO=27
LED=17
IP=4


#################################################
# Thermal Printer Settings
#################################################

# The printer's baud rate
PRINTER_BAUDRATE=19200


#################################################
# Photo Preview Settings
#################################################

# The time in milliseconds for which the camera live preview is running.
PREVIEW_TIMEOUT=30000 #milliseconds


#################################################
# Photo Printout Settings
#################################################

EVENT="Event XYZ"
# The caption which is printed with every photo
PHOTO_CAPTION="/dev/pola @"$EVENT
DATE_PLUS_TIME=true


#################################################
# Local Raspberry Pi Settings
#################################################

# The directory which contains this script along with the /dev/pola image
LOCAL_HOME_DIR="/home/pi/devpola/setup/"

# The directory where the photos get stored. If you don't want to keep them you can just change it to "/tmp/".
LOCAL_PHOTO_DIR="/home/pi/devpola/photos/" 

# A directory where intermediate files are stored. Preferably within /tmp directory.
LOCAL_TMP_DIR="/tmp/devpola/"


#################################################
# Automatic Photo Upload Settings (optional)
#################################################

UPLOAD_ENABLED=false
UPLOAD_INTERVAL_SECONDS=10

# The website where /dev/pola photos will be uploaded to.
UPLOAD_URI_SHORT="devpola.devlol.org"
UPLOAD_URI="https://"$UPLOAD_URI_SHORT"/"
UPLOAD_DIR="pics/event-xyz/"

# SSH Settings
SSH_HOST="devpola-upload"
SSH_UPLOAD_DIR="public_html/"$UPLOAD_DIR

# set the following to `true` if you rather want to have html embeding your photo than the plain photo link
UPLOAD_HTML_ENABLED=false
UPLOAD_HTML_CSS=$DEVPOLA_URI"/ext/default.css"


#################################################
# Automatic WIFI deactivation
#################################################
AUTOMATIC_WIFI_DEACTIVATION=false # set this to true in case you want the WIFI to be automatically disabled during a pre-defined time of being idle
UPLOAD_IDLE_TIMEOUT_CNT_BEFORE_DEACTIVATING_WIFI=30 # * UPLOAD_INTERVAL_SECONDS


